import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'

import 'element-ui/lib/theme-chalk/index.css'


Vue.config.productionTip = false

/* eslint-disable no-new */
//runtime
Vue.use(ElementUI)
new Vue({
  render: h => h(App)
}).$mount("#app")
