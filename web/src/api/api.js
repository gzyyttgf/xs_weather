import request from '@/utils/request'

export function fileUpload(data){
    return request({
        url: '/fileUpload',
        method: 'post',
        data: data
      })
}

export function get(data){
    return request({
        url: '/get',
        method: 'get',
        data: data
      })
}

export function fileDown(data){
  return request({
      url: '/fileDown',
      method: 'post',
      data: data
    })
}